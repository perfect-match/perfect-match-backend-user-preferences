package sdm.perfectmatch.user_preferences.entidades;

import java.math.BigInteger;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "user-preferences")
public class UserPreferences {
	private BigInteger userId;
	private String entertaimentPrefs;
	private String foodPrefs;
	private String musicPrefs;
	private String politicsQuestion;
	private String relationshipPrefs;

	public UserPreferences() {
	}

	public UserPreferences(BigInteger userId, String entertaimentPrefs, String foodPrefs, String musicPrefs, String politicsQuestion,
			String relationshipPrefs) {
		super();
		this.userId = userId;
		this.entertaimentPrefs = entertaimentPrefs;
		this.foodPrefs = foodPrefs;
		this.musicPrefs = musicPrefs;
		this.politicsQuestion = politicsQuestion;
		this.relationshipPrefs = relationshipPrefs;
	}
	
	@DynamoDBHashKey
	public BigInteger getUserId() {
		return userId;
	}
	
	@DynamoDBAttribute
	public String getEntertaimentPrefs() {
		return entertaimentPrefs;
	}
	
	@DynamoDBAttribute
	public String getFoodPrefs() {
		return foodPrefs;
	}

	@DynamoDBAttribute
	public String getMusicPrefs() {
		return musicPrefs;
	}

	@DynamoDBAttribute
	public String getPoliticsQuestion() {
		return politicsQuestion;
	}

	@DynamoDBAttribute
	public String getRelationshipPrefs() {
		return relationshipPrefs;
	}
	
	public void setUserId(BigInteger userId) {
		this.userId = userId;
	}
	
	public void setEntertaimentPrefs(String entertaimentPrefs) {
		this.entertaimentPrefs = entertaimentPrefs;
	}

	public void setFoodPrefs(String foodPrefs) {
		this.foodPrefs = foodPrefs;
	}

	public void setMusicPrefs(String musicPrefs) {
		this.musicPrefs = musicPrefs;
	}

	public void setPoliticsQuestion(String politicsQuestion) {
		this.politicsQuestion = politicsQuestion;
	}

	public void setRelationshipPrefs(String relationshipPrefs) {
		this.relationshipPrefs = relationshipPrefs;
	}

	@Override
	public String toString() {
		return "UserPreferences [entertaimentPrefs=" + entertaimentPrefs + ", foodPrefs=" + foodPrefs + ", musicPrefs="
				+ musicPrefs + ", politicsQuestion=" + politicsQuestion + ", relationshipPrefs=" + relationshipPrefs
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entertaimentPrefs == null) ? 0 : entertaimentPrefs.hashCode());
		result = prime * result + ((foodPrefs == null) ? 0 : foodPrefs.hashCode());
		result = prime * result + ((musicPrefs == null) ? 0 : musicPrefs.hashCode());
		result = prime * result + ((politicsQuestion == null) ? 0 : politicsQuestion.hashCode());
		result = prime * result + ((relationshipPrefs == null) ? 0 : relationshipPrefs.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserPreferences other = (UserPreferences) obj;
		if (entertaimentPrefs == null) {
			if (other.entertaimentPrefs != null)
				return false;
		} else if (!entertaimentPrefs.equals(other.entertaimentPrefs))
			return false;
		if (foodPrefs == null) {
			if (other.foodPrefs != null)
				return false;
		} else if (!foodPrefs.equals(other.foodPrefs))
			return false;
		if (musicPrefs == null) {
			if (other.musicPrefs != null)
				return false;
		} else if (!musicPrefs.equals(other.musicPrefs))
			return false;
		if (politicsQuestion == null) {
			if (other.politicsQuestion != null)
				return false;
		} else if (!politicsQuestion.equals(other.politicsQuestion))
			return false;
		if (relationshipPrefs == null) {
			if (other.relationshipPrefs != null)
				return false;
		} else if (!relationshipPrefs.equals(other.relationshipPrefs))
			return false;
		return true;
	}

}
