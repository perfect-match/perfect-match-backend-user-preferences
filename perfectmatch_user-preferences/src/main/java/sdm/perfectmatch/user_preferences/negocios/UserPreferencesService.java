package sdm.perfectmatch.user_preferences.negocios;

import java.lang.invoke.MethodHandles;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections4.IteratorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import sdm.perfectmatch.user_preferences.entidades.UserPreferences;
import sdm.perfectmatch.user_preferences.persistencia.UserPreferencesRepository;

@Service
public class UserPreferencesService {

	private static final Logger logger= LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	private final UserPreferencesRepository userPrefsRepo;
	
	public UserPreferencesService(UserPreferencesRepository userPrefsRepo) {
		this.userPrefsRepo = userPrefsRepo;
	}
	
	public List<UserPreferences> getAllUserPrefs() {
		if(logger.isInfoEnabled()) {
			logger.info("Busacando todas as preferencias de usuário");
		}
		Iterable<UserPreferences> lista = this.userPrefsRepo.findAll();
		if (lista == null) {
        	return new ArrayList<UserPreferences>();
        }
		return IteratorUtils.toList(lista.iterator());
	}
	
	public UserPreferences getUserPrefsById(BigInteger userId) {
		if(logger.isInfoEnabled()){
            logger.info("Buscando User com o codigo {}",userId);
        }
        Optional<UserPreferences> retorno = this.userPrefsRepo.findById(userId);
        if(!retorno.isPresent()){
            throw new RuntimeException("As preferências do user com o id"+userId+" nao encontrada");
        }
        return retorno.get();
	}
	
	public UserPreferences saveUser(UserPreferences userPrefs){
        if(logger.isInfoEnabled()){
            logger.info("Salvando as preferencias de usuário {}",userPrefs.toString());
        }
        return this.userPrefsRepo.save(userPrefs);
    }
	
	public void deleteUser(BigInteger userId){
        if(logger.isInfoEnabled()){
            logger.info("Excluindo as preferencias de usuuáiro com o id igual a {}",userId);
        }
        this.userPrefsRepo.deleteById(userId);
    }
	
	public boolean isUserExists(BigInteger userId){
    	Optional<UserPreferences> retorno = this.userPrefsRepo.findById(userId);
        return retorno.isPresent() ? true:  false;
    }

}