package sdm.perfectmatch.user_preferences.rest;

import java.math.BigInteger;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import sdm.perfectmatch.user_preferences.entidades.UserPreferences;
import sdm.perfectmatch.user_preferences.negocios.UserPreferencesService;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, path = "user-preferences")
public class UserPreferencesController {
	
	private final UserPreferencesService userPrefsService;
	
	public UserPreferencesController(UserPreferencesService userPrefsService) {
		this.userPrefsService = userPrefsService;
	}
	
	@GetMapping
    public List<UserPreferences> getAllUserPrefs(){
        return userPrefsService.getAllUserPrefs();
    }
    
    @GetMapping(value="{userId}")
    public UserPreferences getUserPrefsById(@PathVariable BigInteger userId) throws Exception{
        if(!ObjectUtils.isEmpty(userId)){
           return userPrefsService.getUserPrefsById(userId);
        }
        throw new Exception("Preferencias de usuário com o id "+userId+" nao encontradas");
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserPreferences createUserPrefs(@RequestBody @NotNull UserPreferences userPrefs) throws Exception {
    	if (userPrefsService.isUserExists(userPrefs.getUserId())) {
    		throw new Exception("User com codigo "+userPrefs.getUserId()+" já existe");
    	} 
        return userPrefsService.saveUser(userPrefs);
    }
    
    @PutMapping(value = "{userId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserPreferences updateUserPrefs(@PathVariable BigInteger userId, 
    		@RequestBody @NotNull UserPreferences newUserPrefs) throws Exception {
    	if (!userId.equals(newUserPrefs.getUserId())) {
    		throw new Exception("O id de usuário "+userId+" nao está correto");
    	}
    	if (!userPrefsService.isUserExists(userId)) {
    		throw new Exception("As preferencias de usuário com o id igual a "+userId+" não existe");
    	}
        return userPrefsService.saveUser(newUserPrefs);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "{userId}")
    public boolean deleteUserPrefs(@PathVariable BigInteger userId) throws Exception {
    	if (!userPrefsService.isUserExists(userId)) {
    		throw new Exception("As preferencias de usuáiro com o id igual a "+userId+" não existe");
    	} 
    	userPrefsService.deleteUser(userId);
        return true;
    }

}
