package sdm.perfectmatch.user_preferences.persistencia;

import java.math.BigInteger;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import sdm.perfectmatch.user_preferences.entidades.UserPreferences;

@EnableScan
public interface UserPreferencesRepository extends CrudRepository<UserPreferences, BigInteger> { 
	UserPreferences findByUserId(BigInteger userId);
}
