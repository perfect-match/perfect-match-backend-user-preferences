package sdm.perfectmatch.user_preferences.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigInteger;
import java.text.ParseException;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;

import sdm.perfectmatch.user_preferences.entidades.UserPreferences;
import sdm.perfectmatch.user_preferences.persistencia.UserPreferencesRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {PropertyPlaceholderAutoConfiguration.class, UserPreferencesTests.DynamoDBConfig.class})
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserPreferencesTests {

	private static Logger LOGGER = LoggerFactory.getLogger(UserPreferencesTests.class);
    
    @Configuration
	@EnableDynamoDBRepositories(basePackageClasses = UserPreferencesRepository.class)
	public static class DynamoDBConfig {

		@Value("${amazon.aws.accesskey}")
		private String amazonAWSAccessKey;

		@Value("${amazon.aws.secretkey}")
		private String amazonAWSSecretKey;

		public AWSCredentialsProvider amazonAWSCredentialsProvider() {
			return new AWSStaticCredentialsProvider(amazonAWSCredentials());
		}

		@Bean
		public AWSCredentials amazonAWSCredentials() {
			return new BasicAWSCredentials(amazonAWSAccessKey, amazonAWSSecretKey);
		}

		@Bean
		public AmazonDynamoDB amazonDynamoDB() {
			return AmazonDynamoDBClientBuilder.standard().withCredentials(amazonAWSCredentialsProvider())
					.withRegion(Regions.US_EAST_1).build();
		}
	}
    
	@Autowired
	private UserPreferencesRepository repository;

	@Test
	public void teste1Criacao() throws ParseException {
		LOGGER.info("Criando objetos...");
		UserPreferences up1 = new UserPreferences(new BigInteger("1"), "terror", "mexicana", "metal","esquerda", "pretendo-casar");
		repository.save(up1);

		UserPreferences up2 = new UserPreferences(new BigInteger("2"), "comedia", "japonesa", "pop","centro", "nao-desejo-relacionamento");
		repository.save(up2);
		
		LOGGER.info("Pesquisado todos");
		Iterable<UserPreferences> lista = repository.findAll();
		assertNotNull(lista.iterator());
		for (UserPreferences UserPrefs : lista) {
			LOGGER.info(UserPrefs.toString());
		}
		LOGGER.info("Pesquisado um objeto");
		UserPreferences result = repository.findByUserId(new BigInteger("1"));
		assertEquals(result, up1);
		assertEquals(result.getMusicPrefs(), "metal");
		LOGGER.info("Encontrado: {}", result);
	}
	
	
	@Test
	public void teste2Exclusao() throws ParseException {
		LOGGER.info("Excluindo objetos...");
		
		UserPreferences up1 = new UserPreferences(new BigInteger("1"), "terror", "mexicana", "metal","esquerda", "pretendo-casar");
		repository.delete(up1);
		
		UserPreferences up2 = new UserPreferences(new BigInteger("40"), "romance", "italiana", "kpop","extrema-direita", "pretendo-casar");
		repository.delete(up2);
		UserPreferences result = repository.findByUserId(new BigInteger("40"));
		
		LOGGER.info("Encontrado: {}", result);
		assertEquals(result, null);
		LOGGER.info("Exclusão feita com sucesso");
	}
}